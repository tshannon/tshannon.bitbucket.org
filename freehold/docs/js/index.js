$(document).ready(function() {
    "use strict";

    Ractive.DEBUG = false;
    var cmRead = new commonmark.Parser();
    var cmWrite = new commonmark.HtmlRenderer();

    var r = new Ractive({
        el: "body",
        template: "#tMain",
        data: {
            docpage: "README",
        }
    });

    if (window.location.hash !== "") {
        r.set("docpage", window.location.hash.slice(1));
    }


    r.observe("docpage", function(newval, oldval, keypath) {
        var url = newval + ".commonmark";
        $.get(url, function(result) {
            r.set("commonmarkParsed", cmWrite.render(cmRead.parse(result)));
        });
    });
});
